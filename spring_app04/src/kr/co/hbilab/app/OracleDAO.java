package kr.co.hbilab.app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class OracleDAO implements CommonDAO{
    Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    StringBuffer sql = new StringBuffer();
    
    @Override
    public Connection connect() {
        String driver = "oracle.jdbc.driver.OracleDriver";
        String url = "jdbc:oracle:thin:@192.168.0.80:1521:orcl";
        String username = "scott";
        String password = "tiger";
        
        try{
            Class.forName(driver);
            conn = DriverManager.getConnection(url, username, password);
            
        }catch(ClassNotFoundException e){
            System.out.println("드라이버 로딩 실패");
        }catch(SQLException e){
            System.out.println("DB 연결 실패");
        }
        
        return conn;
    }
    
    @Override
    public ArrayList<DeptDTO> selectAll() {
        System.out.println("Oracle 전체조회 메서드");
        return new ArrayList<DeptDTO>();
    }
    
}
