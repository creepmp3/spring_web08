package kr.co.hbilab.app;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

//20150527
public class TestMain {
    public static void main(String[] args) {
        ApplicationContext ctx = new GenericXmlApplicationContext("app.xml");
        Dao dao = ctx.getBean("dao", Dao.class);
        List<EmpDTO> list = dao.selectAll();
        

        for(EmpDTO dto : list){
            int lv = dto.getLv()>2?5-dto.getLv():dto.getLv();
            if(dto.getLv()==1) lv = 3;
            String str = "";
            for(int i=0; i<lv; i++){
                str += "\t";
            }
            System.out.print(dto.getEmpno()+"\t");
            System.out.print(dto.getEname()+str);
            System.out.print(dto.getJob()+"\t");
            System.out.print(dto.getMgr()+"\t");
            System.out.print(dto.getHiredate()+"\t");
            System.out.print(dto.getSal()+"\t");
            System.out.print(dto.getComm()+"\t");
            System.out.print(dto.getDeptno()+"\n");
        }
        
        System.out.println("--------------------------------------------------------------------------");
        

        EmpDTO dto = dao.selectOne(7788);
        
        System.out.println("사원번호\t사원명\t직업\t\tMgr\t입사일\t\t급여\tComm\t부서번호");
        
        System.out.print(dto.getEmpno()+"\t");
        System.out.print(dto.getEname()+"\t");
        System.out.print(dto.getJob()+"\t");
        System.out.print(dto.getMgr()+"\t");
        System.out.print(dto.getHiredate()+"\t");
        System.out.print(dto.getSal()+"\t");
        System.out.print(dto.getComm()+"\t");
        System.out.print(dto.getDeptno()+"\n");
        
        System.out.println("--------------------------------------------------------------------------");
        
        /*
        EmpDTO dto2 = new EmpDTO();
        dto2.setEmpno(7789);
        dto2.setEname("SCOTT2");
        dto2.setJob("ALALYST");
        dto2.setMgr(7788);
        dto2.setSal(2900);
        dto2.setComm(1);
        dto2.setDeptno(20);
        
        dao.insertOne(dto2);
        */
        
        /*
        EmpDTO dto3 = new EmpDTO();
        dto3.setEmpno(7789);
        dto3.setEname("SCOTT3");
        dto3.setJob("ALALYST");
        dto3.setMgr(7788);
        dto3.setSal(2800);
        dto3.setComm(2);
        dto3.setDeptno(20);
        
        dao.updateOne(dto3);
        */
        
        /*
        dao.deleteOne(7789);
        */
    }
}
