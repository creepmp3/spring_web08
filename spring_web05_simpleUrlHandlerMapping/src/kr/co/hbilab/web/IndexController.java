package kr.co.hbilab.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
 * 2015. 5. 29.
 * @author KDY
 * 
 */
public class IndexController implements Controller{

    @Override
    public ModelAndView handleRequest(HttpServletRequest arg0, HttpServletResponse arg1)
            throws Exception {
        
        return new ModelAndView("/index/index", "msg", "Index") ;
    }
    
}
