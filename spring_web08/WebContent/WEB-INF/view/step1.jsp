<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.js"></script>
<script type="text/javascript">
$(function(){
	$("#btn").on("click", function(){
		if(!$("#agree").prop("checked")){
			alert("동의를 하셔야 다음으로 진행됩니다.");
		}else{
			$("#frm").attr({"action":"step2.do", "method":"post"}).submit();
		}
	});
});
/* 
window.onload = function(){
	document.getElementById("btn").onclick = check;
}
function check(){
	var agree = document.getElementById("agree").checked;
	if(agree){
        var frm = document.getElementById("frm");
        frm.action = "step2.do";
        frm.method = "post";
        frm.submit();
	}else{
		alert("동의를 하셔야 다음으로 진행됩니다");
    }
}
*/
</script>
<title>Insert title here</title>
</head>
<body>
    <h3>회원가입 - Step1</h3>
    
    <textarea rows="10" cols="30">약관
    당신의 소중한 개인 정보는 
    우리회사의 이익을 위해
    마음대로 사용하겠습니다..
    
    개인정보 냠냠.. 감사합니다.
    </textarea>
    <form id="frm">
        <input type="checkbox" name="ck" id="agree"> 약관동의 
        <input type="button" value="다음" id="btn" />
    </form>
    
</body>
</html>