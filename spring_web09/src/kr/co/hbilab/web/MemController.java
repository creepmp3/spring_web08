package kr.co.hbilab.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 2015. 6. 2.
 * @author KDY
 */

@Controller
@RequestMapping(value="/member")
public class MemController {
    
    @Autowired
    Dao dao;
    
    public void setDao(Dao dao) {
        this.dao = dao;
    }


    @RequestMapping(value="/list")
    public String selectAll(Model model, HttpServletRequest req){
        model.addAttribute("list", dao.selectAll());
        model.addAttribute("url", req.getRequestURI());
        return "/member/list";
    }
}
